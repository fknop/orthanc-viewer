QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = orthanc-viewer
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

LIBS += /usr/local/lib/libvtk* -ldl

INCLUDEPATH += /usr/local/include/vtk-6.2
INCLUDEPATH += /usr/include/boost
INCLUDEPATH += /usr/include/orthanc


HEADERS += \
    Controller/DisplayInterface.h \
    Controller/FusionDialog.h \
    Controller/LoadSeriesThread.h \
    Controller/MergedSeriesInterface.h \
    Controller/OrthancConnectionDialog.h \
    Controller/OrthancDialog.h \
    Controller/SeriesInterface.h \
    Controller/SliceSubInterface.h \
    Controller/SubInterface.h \
    Controller/ViewerWindow.h \
    Controller/VolumeSubInterface.h \
    Model/Colormap.h \
    Model/ProgramConfiguration.h \
    Model/Range.h \
    Model/SeriesData.h \
    Model/Vector3D.h \
    Model/ViewConfiguration.h \
    View/Qt/customwidget/CheckBox.h \
    View/Qt/customwidget/ComboBox.h \
    View/Qt/customwidget/Dial.h \
    View/Qt/customwidget/Dialog.h \
    View/Qt/customwidget/DoubleSpinBox.h \
    View/Qt/customwidget/GroupBox.h \
    View/Qt/customwidget/Label.h \
    View/Qt/customwidget/LineEdit.h \
    View/Qt/customwidget/MainWindow.h \
    View/Qt/customwidget/Menu.h \
    View/Qt/customwidget/MenuBar.h \
    View/Qt/customwidget/ProgressBar.h \
    View/Qt/customwidget/PushButton.h \
    View/Qt/customwidget/RadioButton.h \
    View/Qt/customwidget/Slider.h \
    View/Qt/customwidget/SpinBox.h \
    View/Qt/customwidget/SplashScreen.h \
    View/Qt/customwidget/Splitter.h \
    View/Qt/customwidget/StandardItem.h \
    View/Qt/customwidget/StatusBar.h \
    View/Qt/customwidget/TabWidget.h \
    View/Qt/customwidget/ToolBar.h \
    View/Qt/customwidget/ToolButton.h \
    View/Qt/customwidget/TreeView.h \
    View/Qt/customwidget/VTKWidget.h \
    View/Qt/customwidget/Widget.h \
    View/Qt/ColorbarWidget.h \
    View/Qt/ColormapWidget.h \
    View/Qt/DoubleSlider.h \
    View/Qt/HounsfieldColormapDialog.h \
    View/Qt/HounsfieldWidget.h \
    View/Qt/OkCancelDialog.h \
    View/Qt/TranslationRotationDialog.h \
    View/Qt/ViewConfigurationDialog.h \
    View/VTK/MergedSeriesSliceViewer.h \
    View/VTK/MergedSeriesViewer.h \
    View/VTK/MergedSeriesVolumeViewer.h \
    View/VTK/SeriesSliceViewer.h \
    View/VTK/SeriesViewer.h \
    View/VTK/SeriesVolumeViewer.h \
    View/VTK/Viewer.h \
    main.h \



SOURCES += \
    Controller/DisplayInterface.cpp \
    Controller/FusionDialog.cpp \
    Controller/LoadSeriesThread.cpp \
    Controller/MergedSeriesInterface.cpp \
    Controller/OrthancConnectionDialog.cpp \
    Controller/OrthancDialog.cpp \
    Controller/SeriesInterface.cpp \
    Controller/SliceSubInterface.cpp \
    Controller/SubInterface.cpp \
    Controller/ViewerWindow.cpp \
    Controller/VolumeSubInterface.cpp \
    Model/Colormap.cpp \
    Model/ProgramConfiguration.cpp \
    Model/Range.cpp \
    Model/SeriesData.cpp \
    Model/Vector3D.cpp \
    Model/ViewConfiguration.cpp \
    View/Qt/customwidget/CheckBox.cpp \
    View/Qt/customwidget/ComboBox.cpp \
    View/Qt/customwidget/Dial.cpp \
    View/Qt/customwidget/Dialog.cpp \
    View/Qt/customwidget/DoubleSpinBox.cpp \
    View/Qt/customwidget/GroupBox.cpp \
    View/Qt/customwidget/Label.cpp \
    View/Qt/customwidget/LineEdit.cpp \
    View/Qt/customwidget/MainWindow.cpp \
    View/Qt/customwidget/Menu.cpp \
    View/Qt/customwidget/MenuBar.cpp \
    View/Qt/customwidget/ProgressBar.cpp \
    View/Qt/customwidget/PushButton.cpp \
    View/Qt/customwidget/RadioButton.cpp \
    View/Qt/customwidget/Slider.cpp \
    View/Qt/customwidget/SpinBox.cpp \
    View/Qt/customwidget/SplashScreen.cpp \
    View/Qt/customwidget/Splitter.cpp \
    View/Qt/customwidget/StandardItem.cpp \
    View/Qt/customwidget/StatusBar.cpp \
    View/Qt/customwidget/TabWidget.cpp \
    View/Qt/customwidget/ToolBar.cpp \
    View/Qt/customwidget/ToolButton.cpp \
    View/Qt/customwidget/TreeView.cpp \
    View/Qt/customwidget/VTKWidget.cpp \
    View/Qt/customwidget/Widget.cpp \
    View/Qt/ColorbarWidget.cpp \
    View/Qt/ColormapWidget.cpp \
    View/Qt/DoubleSlider.cpp \
    View/Qt/HounsfieldColormapDialog.cpp \
    View/Qt/HounsfieldWidget.cpp \
    View/Qt/OkCancelDialog.cpp \
    View/Qt/TranslationRotationDialog.cpp \
    View/Qt/ViewConfigurationDialog.cpp \
    View/VTK/MergedSeriesSliceViewer.cpp \
    View/VTK/MergedSeriesViewer.cpp \
    View/VTK/MergedSeriesVolumeViewer.cpp \
    View/VTK/SeriesSliceViewer.cpp \
    View/VTK/SeriesViewer.cpp \
    View/VTK/SeriesVolumeViewer.cpp \
    View/VTK/Viewer.cpp \
    main.cpp \
